import mock
import pytest
import pytz

import scanpc.constants as constant
import scanpc.utils


def test_get_datetime_now():
    with mock.patch.dict(
        "scanpc.utils.os.environ", {"TIMEZONE": "Europe/Dublin"}, clear=True
    ):
        with mock.patch.dict(
            "scanpc.utils.os.environ", {"TIME_FORMAT": "%Y-%m-%d %H:%M:%S"}, clear=True
        ):
            assert scanpc.utils.get_datetime_now()


def test_get_datetime_now_no_env_vars_set(monkeypatch):
    """This is to ensure if the related env vars are not set, the function will
    still work by using the default values.
    """
    monkeypatch.delenv("TIMEZONE", raising=False)
    assert scanpc.utils.get_datetime_now()


def test_get_datetime_from_timestamp():
    with mock.patch.dict(
        "scanpc.utils.os.environ", {"TIME_FORMAT": "%Y-%m-%d %H:%M:%S"}, clear=True
    ):
        assert (
            scanpc.utils.get_datetime_from_timestamp(1610740293.60688)
            == "2021-01-15 19:51:33"
        )


def test_get_host_data():
    with mock.patch("scanpc.utils.socket") as mock_socket:
        mock_socket.gethostname.return_value = "rickylinton-desktop"
        mock_socket.gethostbyname.return_value = "192.168.0.21"
        assert scanpc.utils.get_host_data() == ("rickylinton-desktop", "192.168.0.21")


def test_get_config_directory():
    with mock.patch.dict(
        "scanpc.utils.os.environ", {"SCANPC_ROOT": "/random/path"}, clear=True
    ):
        assert scanpc.utils.get_config_directory() == "/random/path/config"


def test_get_config_directory_env_var_not_set(monkeypatch):
    """This is to ensure if the SCANPC_ROOT env variable is not set, the function will
    still work by using the default value.
    """
    monkeypatch.delenv("SCANPC_ROOT", raising=False)
    assert scanpc.utils.get_config_directory() == "/usr/local/scanpc/config"


@pytest.fixture
def workstation():
    with mock.patch.dict(
        "scanpc.utils.os.environ",
        {"HOST_LOCAL_MAC_ADDRESS": "28:c6:3f:1b:61:24"},
        clear=True,
    ):
        with mock.patch("scanpc.utils.scapy") as mock_scapy:
            mock_ether = mock.Mock(name="ether")
            mock_ether.src = "28:c6:3f:1b:61:24"
            mock_scapy.all.Ether.return_value = mock_ether
            with mock.patch("scanpc.utils.socket") as mock_socket:
                mock_socket.gethostname.return_value = "rickylinton-desktop"
                with mock.patch.dict(
                    "scanpc.utils.os.environ",
                    {"HOST_LOCAL_IP": "192.168.0.21"},
                    clear=True,
                ):
                    with mock.patch("scanpc.utils.getpass") as mock_getpass:
                        mock_getpass.getuser.return_value = "rickylinton"
                        with mock.patch("scanpc.utils.platform") as mock_platform:
                            mock_platform.linux_distribution.return_value = (
                                "CentOS Linux",
                                "8.3.2011",
                                "",
                            )
                            with mock.patch("scanpc.utils.psutil") as mock_psutil:
                                mock_vm = mock.Mock(name="vm")
                                mock_vm.total = 16743055360
                                mock_psutil.virtual_memory.return_value = mock_vm
                                with mock.patch(
                                    "scanpc.utils.multiprocessing"
                                ) as mock_multipro:
                                    mock_multipro.cpu_count.return_value = 12
                                    with mock.patch(
                                        "scanpc.utils.cpuinfo"
                                    ) as mock_cpuinfo:
                                        mock_cpuinfo.get_cpu_info.return_value = {
                                            "brand_raw": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz"
                                        }
                                        return scanpc.utils.Workstation()


def test_workstation_mac_address(workstation):
    assert workstation.mac_address == "28:c6:3f:1b:61:24"


def test_workstation_hostname(workstation):
    assert workstation.hostname == "rickylinton-desktop"


def test_workstation_username(workstation):
    assert workstation.username == "rickylinton"


def test_workstation_host(workstation):
    assert workstation.host == "192.168.0.21"


def test_workstation_os(workstation):
    assert workstation.os == "CentOS Linux-8.3.2011-"


def test_workstation_ram(workstation):
    assert workstation.ram == 16743055360


def test_workstation_cpu_count(workstation):
    assert workstation.cpu_count == 12


def test_workstation_cpu_name(workstation):
    assert workstation.cpu_name == "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz"


def test_workstation_fields(workstation):
    assert workstation.fields == {
        "username": "rickylinton",
        "hostname": "rickylinton-desktop",
        "ram": 16743055360,
        "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
        "cpu_count": 12,
        "mac_address": "28:c6:3f:1b:61:24",
        "host": "192.168.0.21",
        "os": "CentOS Linux-8.3.2011-",
    }


def test_workstation_string_magic_method(workstation):
    assert (
        str(workstation)
        == "Workstation(mac_address=28:c6:3f:1b:61:24, hostname=rickylinton-desktop, username=rickylinton, host=192.168.0.21, os=CentOS Linux-8.3.2011-, ram=16743055360, cpu_count=12, cpu_name=Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz, )"
    )

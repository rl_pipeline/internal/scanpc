import scanpc.constants as constant


def test_entity_abstract_domain(abstract_entity, primary_key):
    instance = abstract_entity(primary_key)
    assert instance.domain == "{scheme}{host}:{port}".format(
        scheme=constant.URL_SCHEME,
        host=primary_key,
        port=constant.SCANPC_SERVER_DEFAULT_PORT,
    )


def test_entity_abstract_beatbot_output(
    abstract_entity, mocked_beatbot_output_entity, primary_key
):
    instance = abstract_entity(primary_key)
    assert instance.beatbot_output == mocked_beatbot_output_entity

import httplib

import mock
import pytest
import requests

import scanpc.constants as constant
import scanpc.client.entity.abstract
import scanpc.client.entity.output


@pytest.fixture
def respond_get_workstations(ws_resources):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return ws_resources

    return Respond()


@pytest.fixture
def respond_get_workstations_data(ws_data_resources):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return ws_data_resources

    return Respond()


@pytest.fixture
def respond_get_workstation(ws_resource):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return ws_resource

    return Respond()


@pytest.fixture
def respond_get_performances(performance_resources):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return performance_resources

    return Respond()


@pytest.fixture
def respond_get_performance(performance_resource):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return performance_resource

    return Respond()


@pytest.yield_fixture
def mocked_workstations_requests(respond_get_workstations):
    with mock.patch("scanpc.client.entity.output.requests") as mock_requests:
        mock_requests.get.return_value = respond_get_workstations
        mock_requests.exceptions.ConnectionError = requests.exceptions.ConnectionError
        yield mock_requests


@pytest.yield_fixture
def mocked_workstation_requests(respond_get_workstation):
    with mock.patch("scanpc.client.entity.output.requests") as mock_requests:
        mock_requests.get.return_value = respond_get_workstation
        mock_requests.exceptions.ConnectionError = requests.exceptions.ConnectionError
        yield mock_requests


@pytest.yield_fixture
def mocked_performances_requests(respond_get_performances):
    with mock.patch("scanpc.client.entity.output.requests") as mock_requests:
        mock_requests.get.return_value = respond_get_performances
        mock_requests.exceptions.ConnectionError = requests.exceptions.ConnectionError
        yield mock_requests


@pytest.yield_fixture
def mocked_performance_requests(respond_get_performance):
    with mock.patch("scanpc.client.entity.output.requests") as mock_requests:
        mock_requests.get.return_value = respond_get_performance
        mock_requests.exceptions.ConnectionError = requests.exceptions.ConnectionError
        yield mock_requests


@pytest.fixture
def primary_key():
    return "192.168.0.21"


@pytest.fixture
def ws_resource(primary_key):
    return {
        "username": "rickylinton",
        "hostname": "rickylinton-desktop",
        "ram": 16743055360,
        "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
        "cpu_count": 12,
        "mac_address": "28:c6:3f:1b:61:24",
        "host": primary_key,
        "os": "CentOS Linux-8.3.2011-",
    }


@pytest.fixture
def ws_resources(ws_resource):
    return [
        ws_resource,
        {
            "username": "rickylinton",
            "hostname": "rickylinton-K43U",
            "ram": 16743055360,
            "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
            "cpu_count": 12,
            "mac_address": "30:c8:3d:4b:62:25",
            "host": "192.168.0.35",
            "os": "CentOS Linux-8.3.2011-",
        },
    ]


@pytest.fixture
def performance_resources(primary_key):
    return {
        primary_key: {"cpu_usage": 27.3, "memory_usage": 21.4},
        "192.168.0.35": {"cpu_usage": 16.4, "memory_usage": 12.8},
    }


@pytest.fixture
def performance_resource():
    return {"cpu_usage": 27.3, "memory_usage": 21.4}


@pytest.fixture
def mocked_beatbot_output_entity():
    mock_entity = mock.Mock(name="output_entity")
    mock_entity.get_heartbeat.return_value = {
        "state": True, "last_connected": 1664923425
    }
    return mock_entity


@pytest.yield_fixture
def mocked_beatbout_output(mocked_beatbot_output_entity):
    with mock.patch(
        "scanpc.client.entity.abstract.beatbot_output"
    ) as mock_beatbot_output:
        mock_beatbot_output.OutputEntity.return_value = mocked_beatbot_output_entity
        yield mock_beatbot_output


@pytest.fixture
def abstract_entity(mocked_beatbout_output):
    return scanpc.client.entity.abstract.AbstractEntity


@pytest.fixture
def output_entity(abstract_entity):
    return scanpc.client.entity.output.OutputEntity


@pytest.yield_fixture
def mocked_tiffany_util():
    with mock.patch("scanpc.client.entity.output.tiffany_util") as mock_tiffany_util:
        mock_tiffany_util.is_debug.return_value = True
        yield mock_tiffany_util


@pytest.yield_fixture
def mocked_logger():
    with mock.patch("scanpc.client.entity.output.logger") as mock_logger:
        yield mock_logger

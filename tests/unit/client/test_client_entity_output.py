import httplib

import mock
import pytest

import scanpc.constants as constant


def test_entity_output_get_workstations(
    output_entity,
    mocked_workstations_requests,
    primary_key,
    mocked_logger,
    mocked_tiffany_util,
    ws_resources,
):
    instance = output_entity(primary_key)
    assert instance.get_workstations() == ws_resources


def test_entity_output_get_workstations_not_found(
    output_entity,
    mocked_workstations_requests,
    primary_key,
    mocked_logger,
    mocked_tiffany_util,
    respond_get_workstations,
):
    instance = output_entity(primary_key)
    respond_get_workstations.status_code = httplib.NOT_FOUND
    assert instance.get_workstations() == []


def test_entity_output_get_workstations_not_implemented_error(
    output_entity, mocked_workstations_requests, primary_key, respond_get_workstations
):
    instance = output_entity(primary_key)
    respond_get_workstations.status_code = httplib.NOT_ACCEPTABLE
    with pytest.raises(NotImplementedError):
        instance.get_workstations()


def test_entity_output_get_workstations_connection_error(
    output_entity, 
    mocked_logger, 
    mocked_workstations_requests, 
    primary_key, 
    respond_get_workstations
):
    instance = output_entity(primary_key)
    mocked_workstations_requests.get.side_effect = (
        mocked_workstations_requests.exceptions.ConnectionError
    )
    with pytest.raises(mocked_workstations_requests.exceptions.ConnectionError):
        instance.get_workstations()


def test_entity_output_get_workstation(
    output_entity,
    mocked_workstation_requests,
    mocked_logger,
    mocked_tiffany_util,
    primary_key,
    ws_resource,
):
    instance = output_entity(primary_key)
    assert instance.get_workstation(primary_key) == ws_resource


def test_entity_output_get_workstation_not_found(
    output_entity,
    mocked_workstation_requests,
    mocked_logger,
    mocked_tiffany_util,
    primary_key,
    respond_get_workstation,
):
    instance = output_entity(primary_key)
    respond_get_workstation.status_code = httplib.NOT_FOUND
    assert instance.get_workstation(primary_key) == {}


def test_entity_output_get_workstation_connection_error(
    output_entity,
    mocked_workstation_requests,
    mocked_logger,
    mocked_tiffany_util,
    primary_key,
    respond_get_workstation,
):
    instance = output_entity(primary_key)
    mocked_workstation_requests.get.side_effect = (
        mocked_workstation_requests.exceptions.ConnectionError
    )
    with pytest.raises(mocked_workstation_requests.exceptions.ConnectionError):
        instance.get_workstation(primary_key)


def test_entity_output_get_performances(
    output_entity, mocked_performances_requests, performance_resources, primary_key
):
    instance = output_entity(primary_key)
    assert instance.get_performances() == performance_resources
    mocked_performances_requests.get.assert_called_with(
        "{domain}{path}".format(domain=instance.domain, path=constant.PERFORMANCES_PATH)
    )


def test_entity_output_get_performances_not_found(
    output_entity, mocked_performances_requests, primary_key, respond_get_performances
):
    instance = output_entity(primary_key)
    respond_get_performances.status_code = httplib.NOT_FOUND
    assert instance.get_performances() == {}


def test_entity_output_get_performances_not_implemented_error(
    output_entity, mocked_performances_requests, primary_key, respond_get_performances
):
    instance = output_entity(primary_key)
    respond_get_performances.status_code = httplib.NOT_ACCEPTABLE
    with pytest.raises(NotImplementedError):
        instance.get_performances()


def test_entity_output_get_performances_connection_error(
    output_entity, 
    mocked_logger, 
    mocked_performances_requests, 
    primary_key, 
    respond_get_performances
):
    instance = output_entity(primary_key)
    mocked_performances_requests.get.side_effect = (
        mocked_performances_requests.exceptions.ConnectionError
    )
    with pytest.raises(mocked_performances_requests.exceptions.ConnectionError):
        instance.get_performances()


def test_entity_output_get_performance(
    output_entity,
    mocked_performance_requests,
    mocked_logger, 
    mocked_tiffany_util,
    primary_key,
    performance_resource,
):
    instance = output_entity(primary_key)
    assert instance.get_performance(primary_key) == performance_resource


def test_entity_output_get_performance_not_found(
    output_entity,
    mocked_performance_requests,
    mocked_logger,
    mocked_tiffany_util,
    primary_key,
    respond_get_performance,
):
    instance = output_entity(primary_key)
    respond_get_performance.status_code = httplib.NOT_FOUND
    assert instance.get_performance(primary_key) == {}


def test_entity_output_get_performance_connection_error(
    output_entity,
    mocked_performance_requests,
    mocked_logger,
    mocked_tiffany_util,
    primary_key,
    respond_get_performance,
):
    instance = output_entity(primary_key)
    mocked_performance_requests.get.side_effect = (
        mocked_performance_requests.exceptions.ConnectionError
    )
    with pytest.raises(mocked_performance_requests.exceptions.ConnectionError):
        instance.get_performance(primary_key)


def test_entity_output_get_workstations(
    monkeypatch,
    output_entity,
    mocked_logger,
    mocked_tiffany_util,
    mocked_performance_requests,
    primary_key,
    ws_resource,
):
    instance = output_entity(primary_key)
    monkeypatch.setattr(instance, "get_workstations", mock.MagicMock(return_value=[ws_resource]))
    assert instance.get_workstations_data() == [
        [
            "28:c6:3f:1b:61:24",
            primary_key,
            "rickylinton-desktop",
            "rickylinton",
            '2022-10-04 22:43:45',
            "Connected",
            27.3,
            21.4,
            12,
            16743055360,
            "CentOS Linux-8.3.2011-",
            "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
        ],
    ]

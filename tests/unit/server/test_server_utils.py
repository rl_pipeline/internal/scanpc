import collections
import httplib
import os

import mock
import pytest

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception
import scanpc.server.utils


def test_get_scanpc_schema_validator(request, mocked_schema):
    with mock.patch.dict(
        "scanpc.server.utils.os.environ",
        {"SCANPC_CONFIG_SERVER": "{}/config".format(request.fspath.dirname)},
        clear=True,
    ):
        assert scanpc.server.utils.get_scanpc_schema_validator() == mocked_schema


def test_ws_resource_manager_get_resources(
    workstations_resource_manager, mocked_ws_model, ws_resources
):
    assert workstations_resource_manager.get_resources() == ws_resources
    mocked_ws_model.get_workstations.assert_called()


def test_ws_resource_manager_get_resources_database_error(
    workstations_resource_manager, mocked_ws_model, ws_resources
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.get_workstations.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.get_resources()
        mock_logger.critical.assert_called()


def test_ws_resource_manager_get_resources_operational_error(
    workstations_resource_manager, mocked_ws_model, ws_resources
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.get_workstations.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.get_resources()
        mock_logger.critical.assert_called()


def test_ws_resource_manager_get_resource(
    workstations_resource_manager, mocked_ws_model, ws_resource, primary_key
):
    assert workstations_resource_manager.get_resource(primary_key) == ws_resource
    mocked_ws_model.get_workstation.assert_called_with(primary_key)


def test_ws_resource_manager_get_resource_database_error(
    workstations_resource_manager, mocked_ws_model, ws_resource, primary_key
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.get_workstation.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.get_resource(primary_key)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_get_resource_operational_error(
    workstations_resource_manager, mocked_ws_model, ws_resource, primary_key
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.get_workstation.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.get_resource(primary_key)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_insert_resource(
    workstations_resource_manager, mocked_ws_model, ws_resource,
):
    assert workstations_resource_manager.insert_resource(ws_resource) == None
    mocked_ws_model.insert_workstation.assert_called_with(ws_resource)


def test_ws_resource_manager_insert_resource_database_error(
    workstations_resource_manager, mocked_ws_model, ws_resource
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.insert_workstation.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.insert_resource(ws_resource)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_insert_resource_operational_error(
    workstations_resource_manager, mocked_ws_model, ws_resource
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.insert_workstation.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.insert_resource(ws_resource)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_update_resource(
    workstations_resource_manager,
    mocked_ws_model,
    primary_key,
    ws_update_one_fields,
    ws_update_one_return,
):
    assert (
        workstations_resource_manager.update_resource(primary_key, ws_update_one_fields)
        == ws_update_one_return
    )
    mocked_ws_model.update_workstation.assert_called_with(
        primary_key, ws_update_one_fields
    )


def test_ws_resource_manager_update_resource_database_error(
    workstations_resource_manager, mocked_ws_model, primary_key, ws_update_one_fields
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.update_workstation.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.update_resource(primary_key, ws_update_one_fields)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_update_resource_database_operational_error(
    workstations_resource_manager, mocked_ws_model, primary_key, ws_update_one_fields
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.update_workstation.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.update_resource(primary_key, ws_update_one_fields)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_delete_resources(
    workstations_resource_manager, mocked_ws_model
):
    assert workstations_resource_manager.delete_resources() == None
    mocked_ws_model.delete_workstations.assert_called()


def test_ws_resource_manager_delete_resources_database_error(
    workstations_resource_manager, mocked_ws_model,
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.delete_workstations.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.delete_resources()
        mock_logger.critical.assert_called()


def test_ws_resource_manager_delete_resources_database_operational_error(
    workstations_resource_manager, mocked_ws_model,
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.delete_workstations.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.delete_resources()
        mock_logger.critical.assert_called()


def test_ws_resource_manager_delete_resource(
    workstations_resource_manager, mocked_ws_model, primary_key, ws_delete_one_return
):
    assert (
        workstations_resource_manager.delete_resource(primary_key)
        == ws_delete_one_return
    )
    mocked_ws_model.delete_workstation.assert_called_with(primary_key)


def test_ws_resource_manager_delete_resource_database_error(
    workstations_resource_manager, mocked_ws_model, primary_key,
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.delete_workstation.side_effect = (
            server_exception.WorkstationsDatabaseError
        )
        workstations_resource_manager.delete_resource(primary_key)
        mock_logger.critical.assert_called()


def test_ws_resource_manager_delete_resource_database_operational_error(
    workstations_resource_manager, mocked_ws_model, primary_key,
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_ws_model.delete_workstation.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstations_resource_manager.delete_resource(primary_key)
        mock_logger.critical.assert_called()


def test_pf_resource_manager_get_resources(
    performances_resource_manager, mocked_pf_model, performance_data
):
    assert performances_resource_manager.get_resources() == performance_data
    mocked_pf_model.get_performances.assert_called()


def test_pf_resource_manager_get_resource(
    performances_resource_manager,
    mocked_pf_model,
    primary_key,
    primary_key_performance_data,
):
    assert (
        performances_resource_manager.get_resource(primary_key)
        == primary_key_performance_data
    )
    mocked_pf_model.get_performance.assert_called_with(primary_key)


def test_pf_resource_manager_upsert_resource(
    performances_resource_manager, mocked_pf_model, primary_key, ws_update_one_fields
):
    assert (
        performances_resource_manager.upsert_resource(primary_key, ws_update_one_fields)
        == None
    )
    mocked_pf_model.upsert_performance.assert_called_with(
        primary_key, ws_update_one_fields
    )


def test_pf_resource_manager_delete_resources(
    performances_resource_manager, mocked_pf_model,
):
    assert performances_resource_manager.delete_resources() == None
    mocked_pf_model.delete_performances.assert_called()


def test_pf_resource_manager_delete_resource(
    performances_resource_manager, mocked_pf_model, primary_key,
):
    assert performances_resource_manager.delete_resource(primary_key) == None
    mocked_pf_model.delete_performance.assert_called_with(primary_key)


def test_ws_resource_manager_delete_resource_key_error(
    performances_resource_manager, mocked_pf_model, primary_key
):
    with mock.patch("scanpc.server.utils.logger") as mock_logger:
        mocked_pf_model.delete_performance.side_effect = (
            server_exception.PerformancesKeyError
        )
        performances_resource_manager.delete_resource(primary_key)
        mock_logger.error.assert_called()

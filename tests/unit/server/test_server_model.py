import collections
import httplib

import mock
import pymongo.errors
import pytest

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception
import scanpc.server.database


def test_workstations_model_init(workstations_model, mocked_pymongo):
    assert workstations_model.__class__.workstations_col == constant.WORKSTATIONS_COL
    mocked_pymongo.PyMongo.return_value.db.workstations.drop.assert_called()
    mocked_pymongo.PyMongo.return_value.db.create_collection.assert_called_with(
        constant.WORKSTATIONS_COL
    )
    mocked_pymongo.PyMongo.return_value.db.command.assert_called()


def test_workstations_model_insert_workstation(
    workstations_model, mocked_pymongo, ws_resource
):
    workstations_model.insert_workstation(ws_resource)
    mocked_pymongo.PyMongo.return_value.db.workstations.insert_one.assert_called_with(
        ws_resource
    )


def test_workstations_model_get_workstations(
    workstations_model, mocked_pymongo, ws_resource
):
    assert workstations_model.get_workstations() == [ws_resource]
    mocked_pymongo.PyMongo.return_value.db.workstations.find.assert_called()


def test_workstations_model_delete_workstations(
    workstations_model, mocked_pymongo, ws_resource
):
    workstations_model.delete_workstations()
    mocked_pymongo.PyMongo.return_value.db.workstations.delete_many.assert_called_with(
        {}
    )


def test_workstations_model_get_workstation(
    workstations_model, mocked_pymongo, ws_resource, primary_key
):
    assert workstations_model.get_workstation(primary_key) == ws_resource
    mocked_pymongo.PyMongo.return_value.db.workstations.find_one.assert_called_with(
        {"_id": primary_key}
    )


def test_workstations_model_update_workstation(
    workstations_model,
    mocked_pymongo,
    ws_update_one_return,
    primary_key,
    ws_update_one_fields,
):
    assert (
        workstations_model.update_workstation(primary_key, {"cpu_count": 5})
        == ws_update_one_return
    )
    mocked_pymongo.PyMongo.return_value.db.workstations.update_one.assert_called_with(
        {"_id": primary_key}, {"$set": ws_update_one_fields}
    )


def test_workstations_model_delete_workstation(
    workstations_model, mocked_pymongo, ws_delete_one_return, primary_key
):
    assert (
        workstations_model.delete_workstation({"_id": primary_key})
        == ws_delete_one_return
    )


def test_performances_model_init(performances_model, performance_data):
    assert performances_model._performance_data == performance_data


def test_performances_model_get_performances(performances_model, performance_resources):
    assert performances_model.get_performances() == performance_resources


def test_performances_model_get_performance(
    performances_model, primary_key, primary_key_performance_data
):
    assert (
        performances_model.get_performance(primary_key) == primary_key_performance_data
    )


def test_performances_model_upsert_performance(performances_model):
    assert (
        performances_model.upsert_performance(
            "192.168.0.40", {"cpu_usage": 30.0, "memory_usage": 15.0}
        )
        == True
    )


def test_performances_model_upsert_performance_key_error(performances_model):
    with pytest.raises(server_exception.PerformancesKeyError):
        performances_model.upsert_performance(
            "192.168.0.25", {"foo": 30.0, "bar": 15.0}
        )


def test_performances_model_delete_performances(performances_model):
    performances_model.delete_performances()
    assert performances_model._performance_data == {}


def test_performances_model_delete_performance(performances_model, primary_key):
    performances_model.delete_performance(primary_key)
    assert performances_model._performance_data == {
        "192.168.0.35": {"cpu_usage": 16.4, "memory_usage": 12.8}
    }


def test_performances_model_delete_performance_key_error(performances_model):
    with pytest.raises(server_exception.PerformancesKeyError):
        performances_model.delete_performance("192.168.0.40")

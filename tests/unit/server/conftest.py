import contextlib

import flask
import flask_restful
import mock
import pytest

import scanpc.constants as constant
import scanpc.server.database


pytest_plugin = "conftest_server"


@pytest.fixture
def flask_app():
    return flask.Flask("flask_app")


@pytest.fixture
def workstations():
    return scanpc.server.api.Workstations()


@pytest.fixture
def workstation():
    return scanpc.server.api.Workstation()


@pytest.fixture
def performances():
    return scanpc.server.api.Performances()


@pytest.fixture
def performance():
    return scanpc.server.api.Performance()


@pytest.yield_fixture
def mocked_pymongo(ws_cursor_return, ws_update_one_return, ws_delete_one_return):
    with mock.patch("scanpc.server.database.flask_pymongo") as mock_pymongo:
        mongo = mock.Mock(name="mongo")
        mongo.db.list_collection_names.return_value = []
        mongo.db.workstations.drop.return_value = None
        mongo.db.workstations.insert_one.return_value = None
        mongo.db.workstations.replace_one.return_value = None
        mongo.db.workstations.find.return_value = [ws_cursor_return]
        mongo.db.workstations.find_one.return_value = ws_cursor_return
        mongo.db.workstations.delete_many.return_value = None

        update_one_result = mock.Mock(name="update_one_result")
        update_one_result.raw_result = ws_update_one_return
        mongo.db.workstations.update_one.return_value = update_one_result

        delete_one_result = mock.Mock(name="delete_one_result")
        delete_one_result.raw_result = ws_delete_one_return
        mongo.db.workstations.delete_one.return_value = delete_one_result

        mongo.db.create_collection.return_value = None
        mongo.db.command.return_value = None
        mock_pymongo.PyMongo.return_value = mongo
        yield mock_pymongo


@pytest.fixture
def workstations_model(flask_app, mocked_pymongo, mocked_server_util):
    return scanpc.server.database.WorkstationsDatabase(flask_app)


@pytest.fixture
def performances_model(performance_data):
    return scanpc.server.database.PerformancesDatabase(performance_data=performance_data)


@pytest.fixture
def mocked_ws_model(
    ws_resources, ws_resource, ws_update_one_return, ws_delete_one_return
):
    ws_model = mock.Mock(name="ws_model")
    ws_model.get_workstations.return_value = ws_resources
    ws_model.delete_workstations.return_value = None
    ws_model.get_workstation.return_value = ws_resource
    ws_model.insert_workstation.return_value = None
    ws_model.update_workstation.return_value = ws_update_one_return
    ws_model.delete_workstation.return_value = ws_delete_one_return
    return ws_model


@pytest.fixture
def mocked_pf_model(performance_data, primary_key_performance_data):
    pf_model = mock.Mock(name="pf_model")
    pf_model.get_performances.return_value = performance_data
    pf_model.get_performance.return_value = primary_key_performance_data
    pf_model.upsert_performance.return_value = None
    pf_model.delete_performances.return_value = None
    pf_model.delete_performance.return_value = None
    return pf_model


@pytest.yield_fixture
@contextlib.contextmanager
def mocked_threading_lock():
    mock_lock = mock.Mock(name="Lock")
    yield mock_lock


@pytest.fixture
def workstations_resource_manager(mocked_ws_model, mocked_threading_lock):
    with mock.patch("scanpc.server.utils.threading") as mock_threading:
        mock_threading.Lock.return_value = mocked_threading_lock
        return scanpc.server.utils.WorkstationsResourceManager(mocked_ws_model)


@pytest.fixture
def performances_resource_manager(mocked_pf_model, mocked_threading_lock):
    with mock.patch("scanpc.server.utils.threading") as mock_threading:
        mock_threading.Lock.return_value = mocked_threading_lock
        return scanpc.server.utils.PerformancesResourceManager(mocked_pf_model)


@pytest.fixture
def mocked_schema():
    return {
        u"$jsonSchema": {
            u"required": [u"mac_address", u"hostname"],
            u"properties": {
                u"hostname": {
                    u"description": u"must be a string and is required",
                    u"bsonType": u"string",
                },
                u"mac_address": {
                    u"description": u"must be a string and is required",
                    u"bsonType": u"string",
                },
            },
            u"bsonType": u"object",
        }
    }


@pytest.yield_fixture
def mocked_server_util(mocked_schema):
    with mock.patch("scanpc.server.database.server_util") as mock_server_util:
        mock_server_util.get_scanpc_schema_validator.return_value = mocked_schema
        yield mock_server_util


@pytest.fixture
def primary_key():
    return "192.168.0.21"


@pytest.fixture
def ws_resource(primary_key):
    return {
        "username": "rickylinton",
        "hostname": "rickylinton-desktop",
        "ram": 16743055360,
        "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
        "cpu_count": 12,
        "mac_address": "28:c6:3f:1b:61:24",
        "host": primary_key,
        "os": "CentOS Linux-8.3.2011-",
    }


@pytest.fixture
def ws_cursor_return(primary_key):
    return {
        "username": "rickylinton",
        "hostname": "rickylinton-desktop",
        "ram": 16743055360,
        "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
        "cpu_count": 12,
        "mac_address": "28:c6:3f:1b:61:24",
        "_id": primary_key,
        "os": "CentOS Linux-8.3.2011-",
    }


@pytest.fixture
def ws_update_one_fields():
    return {"cpu_count": 5}


@pytest.fixture
def ws_update_one_return():
    return {"updatedExisting": True, "nModified": 1, "ok": 1.0, "n": 1}


@pytest.fixture
def ws_delete_one_return():
    return {"ok": 1.0, "n": 1}


@pytest.fixture
def ws_resources(ws_resource):
    return [
        ws_resource,
        {
            "username": "rickylinton",
            "hostname": "rickylinton-K43U",
            "ram": 16743055360,
            "cpu_name": "Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz",
            "cpu_count": 12,
            "mac_address": "30:c8:3d:4b:62:25",
            "host": "192.168.0.35",
            "os": "CentOS Linux-8.3.2011-",
        },
    ]


@pytest.fixture
def performance_resources(primary_key):
    return {
        primary_key: {"cpu_usage": 27.3, "memory_usage": 21.4},
        "192.168.0.35": {"cpu_usage": 16.4, "memory_usage": 12.8},
    }


@pytest.fixture
def primary_key_performance_data():
    return {"cpu_usage": 27.3, "memory_usage": 21.4}


@pytest.fixture
def performance_data(primary_key, primary_key_performance_data):
    return {
        primary_key: primary_key_performance_data,
        "192.168.0.35": {"cpu_usage": 16.4, "memory_usage": 12.8},
    }


@pytest.fixture
def performance_resource():
    return {"cpu_usage": 27.3, "memory_usage": 21.4}


@pytest.yield_fixture
def mocked_ws_resources_manager(ws_resources):
    with mock.patch("scanpc.server.api.Workstations.manager") as mock_manager:
        mock_manager.get_resources.return_value = ws_resources
        mock_manager.insert_resource.return_value = None
        mock_manager.delete_resources.return_value = None
        yield mock_manager


@pytest.yield_fixture
def mocked_ws_resource_manager(ws_resource, ws_delete_one_return, ws_update_one_return):
    with mock.patch("scanpc.server.api.Workstation.manager") as mock_manager:
        mock_manager.get_resource.return_value = ws_resource
        mock_manager.update_resource.return_value = ws_update_one_return
        mock_manager.delete_resource.return_value = ws_delete_one_return
        yield mock_manager


@pytest.yield_fixture
def mocked_performance_resources_manager(performance_resources):
    with mock.patch("scanpc.server.api.Performances.manager") as mock_manager:
        mock_manager.get_resources.return_value = performance_resources
        mock_manager.upsert_resource.return_value = None
        mock_manager.delete_resources.return_value = None
        yield mock_manager


@pytest.yield_fixture
def mocked_performance_resource_manager(performance_resource):
    with mock.patch("scanpc.server.api.Performance.manager") as mock_manager:
        mock_manager.get_resource.return_value = performance_resource
        mock_manager.upsert_resource.return_value = None
        yield mock_manager


@pytest.fixture
def parse_post_args_return_object(ws_resource):
    return mock.Mock(return_value=ws_resource)


@pytest.fixture
def parse_post_args_return_object_copy(ws_resource):
    return ws_resource


@pytest.yield_fixture
def mocked_request():
    with mock.patch("scanpc.server.api.request") as mocked_request:
        mocked_request.base_url.return_value = (
            "http://192.168.0.21:5000/scanpc/workstations"
        )
        yield mocked_request


@pytest.yield_fixture
def mocked_flask_abort():
    with mock.patch("scanpc.server.api.flask.abort") as mock_flask_abort:
        yield mock_flask_abort


@pytest.yield_fixture
def mocked_tiffany_util():
    with mock.patch("scanpc.server.api.tiffany_util") as mock_tiffany_util:
        mock_tiffany_util.is_debug.return_value = True
        yield mock_tiffany_util


@pytest.fixture
def ws_resource_with_url(ws_resource, mocked_request):
    ws_resource["url"] = mocked_request.base_url
    return ws_resource


@pytest.fixture
def parse_put_args_return_object(ws_resource):
    mock_obj = mock.Mock(return_value=ws_resource)
    mock_obj.items.return_value = ws_resource.items()
    return mock_obj


@pytest.fixture
def parse_performance_post_args_return_object(primary_key, performance_resource):
    performance_resource[constant.PRIMARY_KEY] = primary_key
    mock_obj = mock.Mock(return_value=performance_resource)
    mock_obj.pop.return_value = primary_key
    return mock_obj


@pytest.fixture
def parse_performance_put_args_return_object(performance_resource):
    return mock.Mock(return_value=performance_resource)

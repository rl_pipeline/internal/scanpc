import httplib
import mock
import pytest

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception
import scanpc.server.api


def test_workstations_get(
    workstations,
    mocked_ws_resources_manager,
    ws_resources,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert workstations.get() == (ws_resources, httplib.OK)
        mocked_ws_resources_manager.get_resources.assert_called()
        mock_logger.debug.assert_called()

        mocked_ws_resources_manager.get_resources = mock.Mock(return_value=None)
        workstations.get()
        mocked_flask_abort.assert_called_with(
            httplib.NOT_FOUND, "No workstations found on database!"
        )


def test_workstations_post(
    workstations,
    mocked_ws_resources_manager,
    parse_post_args_return_object,
    parse_post_args_return_object_copy,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.parse_post_args") as mock_parse_post_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            parse_post_args_return_object.copy = mock.Mock(
                return_value=parse_post_args_return_object_copy
            )
            mock_parse_post_args.return_value = (
                parse_post_args_return_object.return_value
            )
            assert workstations.post() == httplib.OK
            mocked_ws_resources_manager.insert_resource.assert_called_with(
                parse_post_args_return_object_copy
            )
            mock_logger.debug.assert_called()

            mocked_ws_resources_manager.insert_resource.side_effect = (
                server_exception.WorkstationsDatabaseFieldsError
            )
            assert workstations.post() == httplib.BAD_REQUEST

            mocked_ws_resources_manager.insert_resource.side_effect = (
                server_exception.WorkstationsDatabaseOperationalError
            )
            assert workstations.post() == httplib.INTERNAL_SERVER_ERROR


def test_workstations_delete(
    workstations, mocked_ws_resources_manager, mocked_flask_abort, mocked_tiffany_util
):
    with mock.patch("scanpc.server.api.parse_post_args") as mock_parse_post_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            assert workstations.delete() == httplib.OK
            mocked_ws_resources_manager.delete_resources.assert_called()
            mock_logger.debug.assert_called()

            mocked_ws_resources_manager.delete_resources.side_effect = (
                server_exception.WorkstationsDatabaseOperationalError
            )
            workstations.delete()
            mocked_flask_abort.assert_called_with(
                httplib.INTERNAL_SERVER_ERROR, "DELETE - Database Operational Error!"
            )


def test_workstation_get(
    workstation,
    mocked_ws_resource_manager,
    ws_resource,
    primary_key,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert workstation.get(primary_key) == (ws_resource, httplib.OK)
        mocked_ws_resource_manager.get_resource.assert_called_with(primary_key)
        mock_logger.debug.assert_called()

        mocked_ws_resource_manager.get_resource = mock.Mock(return_value=None)
        workstation.get(primary_key)
        mocked_flask_abort.assert_called_with(
            httplib.NOT_FOUND,
            "GET - Workstation {} was not found in the collection!".format(primary_key),
        )


def test_workstation_put(
    workstation,
    mocked_ws_resource_manager,
    parse_put_args_return_object,
    ws_resource,
    ws_resource_with_url,
    primary_key,
    mocked_request,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.parse_put_args") as mock_parse_put_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            mock_parse_put_args.return_value = parse_put_args_return_object

            assert workstation.put(primary_key) == (ws_resource_with_url, httplib.OK)
            mocked_ws_resource_manager.update_resource.assert_called_with(
                primary_key, ws_resource
            )
            mock_logger.debug.assert_called()

            mocked_ws_resource_manager.update_resource.side_effect = (
                server_exception.WorkstationsDatabaseOperationalError
            )
            workstation.put(primary_key)
            mocked_flask_abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_workstation_put_no_modified(
    workstation,
    mocked_ws_resource_manager,
    parse_put_args_return_object,
    primary_key,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.parse_put_args") as mock_parse_put_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            mock_parse_put_args.return_value = parse_put_args_return_object
            mocked_ws_resource_manager.update_resource = mock.Mock(
                return_value={"nModified": 0}
            )
            assert workstation.put(primary_key) == httplib.NO_CONTENT


def test_workstation_delete(
    workstation,
    mocked_ws_resource_manager,
    primary_key,
    mocked_tiffany_util,
    mocked_request,
    mocked_flask_abort,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert workstation.delete(primary_key) == httplib.OK
        mocked_ws_resource_manager.delete_resource.assert_called_with(primary_key)
        mock_logger.debug.assert_called()

        mocked_ws_resource_manager.delete_resource.side_effect = (
            server_exception.WorkstationsDatabaseOperationalError
        )
        workstation.delete(primary_key)
        mocked_flask_abort.assert_called_with(
            httplib.INTERNAL_SERVER_ERROR, "DELETE - Database Operational Error!"
        )


def test_performances_get(
    performances,
    mocked_performance_resources_manager,
    performance_resources,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert performances.get() == (performance_resources, httplib.OK)
        mocked_performance_resources_manager.get_resources.assert_called()
        mock_logger.debug.assert_called()

        mocked_performance_resources_manager.get_resources = mock.Mock(
            return_value=None
        )
        performances.get()
        mocked_flask_abort.assert_called_with(
            httplib.NOT_FOUND, "GET - No performances data available!"
        )


def test_performances_post(
    performances,
    mocked_performance_resources_manager,
    parse_performance_post_args_return_object,
    mocked_tiffany_util,
    primary_key,
):
    with mock.patch(
        "scanpc.server.api.parse_performance_post_args"
    ) as mock_performance_parse_post_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            mock_performance_parse_post_args.return_value = (
                parse_performance_post_args_return_object
            )
            assert performances.post() == httplib.OK
            mocked_performance_resources_manager.upsert_resource.assert_called_with(
                primary_key, parse_performance_post_args_return_object
            )
            mock_logger.debug.assert_called()


def test_performances_delete(
    performances,
    mocked_performance_resources_manager,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert performances.delete() == httplib.OK
        mocked_performance_resources_manager.delete_resources.assert_called()
        mock_logger.debug.assert_called()


def test_performance_get(
    performance,
    mocked_performance_resource_manager,
    performance_resource,
    primary_key,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert performance.get(primary_key) == (performance_resource, httplib.OK)
        mocked_performance_resource_manager.get_resource.assert_called_with(primary_key)
        mock_logger.debug.assert_called()

        mocked_performance_resource_manager.get_resource = mock.Mock(return_value=None)
        performance.get(primary_key)
        mocked_flask_abort.assert_called_with(
            httplib.NOT_FOUND,
            "GET - mac address {} was not found in the collection!".format(primary_key),
        )


def test_performance_put(
    performance,
    mocked_performance_resource_manager,
    parse_performance_put_args_return_object,
    performance_resource,
    primary_key,
    mocked_flask_abort,
    mocked_tiffany_util,
):
    with mock.patch(
        "scanpc.server.api.parse_performance_put_args"
    ) as parse_performance_put_args:
        with mock.patch("scanpc.server.api.logger") as mock_logger:
            parse_performance_put_args.return_value = (
                parse_performance_put_args_return_object
            )
            assert performance.put(primary_key) == (
                parse_performance_put_args_return_object,
                httplib.OK,
            )
            mocked_performance_resource_manager.upsert_resource.assert_called_with(
                primary_key, parse_performance_put_args_return_object
            )
            mock_logger.debug.assert_called()


def test_performance_delete(
    performance,
    mocked_performance_resource_manager,
    primary_key,
    mocked_tiffany_util,
    mocked_flask_abort,
    mocked_request,
):
    with mock.patch("scanpc.server.api.logger") as mock_logger:
        assert performance.delete(primary_key) == httplib.OK
        mocked_performance_resource_manager.delete_resource.assert_called_with(
            primary_key
        )
        mock_logger.debug.assert_called()

        mocked_performance_resource_manager.delete_resource.side_effect = KeyError
        assert performance.delete(primary_key) == httplib.NOT_FOUND

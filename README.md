# [scanpc](https://gitlab.com/rl_pipeline/internal/scanpc.git)

### Overview
Scanpc is a web application with a client-server model to help monitoring
the heartbeat using the [beatbot service](https://gitlab.com/rl_pipeline/internal/beatbot) and the performance of all the connected client to the server.

##### Scanpc Server
* The scanpc server is run on top Flask web framework.
* RESTful API that uses REST architecture to handle request (CRUD operations) 
  on the frontend.
* The API has six endpoints: 
  * /scanpc/workstations
  * /scanpc/workstations/<string: host>
  * /scanpc/performances
  * /scanpc/performances/<string: host>
  * /beatbot/hosts
  * /beatbot/hosts/<string: host>
* Uses MongoDB for the persistent database.

##### Scanpc Client
* The client sends a post request one time to the server with detailed information 
  about the client after the first connection is established.
* The client sends a routine post requests to the server with some performance
  information of the client within an interval of time.
* Supports client API to perform requests to the server.

##### Scanpc Monitor
* The monitor displays detailed information of all the connected clients
  on the graphical user inteface and writen with PySide2 python module.
  
### Disclaimer
This was built and tested under:
* cmake-3.14.7
* cmake_modules-1.0.1
* docker-19.03.12
* docker-compose 1.26.2
* GNU Make 4.1
* python-2.7.18
* Ubuntu 18.04.3 LTS

### Prerequisites
* GNU Make 4
* cmake-3.14
* docker-19
* docker-compose-1.26

### How to Build Docker Image
```bash
git clone https://gitlab.com/rl_pipeline/internal/scanpc.git
cd scanpc
git checkout tags/<tagname>
```

###### Build the server docker image
```bash
./bootstraps/build_server.sh
```

###### Build the client docker image
```bash
./bootstraps/build_client.sh
```

### How to Run Server Container
###### Create docker network
```bash
docker network create scanpcnet
```

###### Run server container
```bash
./bootstraps/run_server.sh
```

### How to Run Client Container
###### Create docker network
```bash
docker network create scanpcnet
```

###### Run client container
```bash
./bootstraps/run_client.sh <your_server_ip_address> <scanpc_server_port>
```

Default scanpc server port is 5000. For example:
```bash
./bootstraps/run_client.sh 192.168.0.21 5000

```

### How to Run Monitor Container
###### Create docker network
```bash
docker network create scanpcnet
```

###### Run monitor container
```bash
./bootstraps/run_client_monitor.sh <your_server_ip_address> <scanpc_server_port>
```

Default scanpc server port is 5000. For example:
```bash
./bootstraps/run_client_monitor.sh 192.168.0.21 5000
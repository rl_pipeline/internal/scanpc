#!/bin/sh

# export UID=$(id -u)
# export GID=$(id -g)
export SERVER_HOST=$1
export SERVER_PORT=$2
export HOSTNAME=$(hostname)
export HOST_LOCAL_IP=$(hostname -I | awk '{print $1}')
export HOST_LOCAL_MAC_ADDRESS=$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address)
docker-compose -f docker-compose-client.yml up
DATABASE = "scanpc"

URL_SCHEME = "http://"

WORKSTATIONS_COL = "workstations"
PERFORMANCES_COL = "performances"

PERFORMANCES_PATH = "/{database}/{collection}".format(
    database=DATABASE, collection=PERFORMANCES_COL
)
WORKSTATIONS_PATH = "/{database}/{collection}".format(
    database=DATABASE, collection=WORKSTATIONS_COL
)

TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
TIMEZONE = "Europe/Dublin"

PRIMARY_KEY = "host"

SCANPC_SERVER_DEFAULT_PORT = 5000
BEATBOT_SERVER_DEFAULT_PORT = 5001

CONNECTED = "Connected"
NOT_CONNECTED = "Not Connected"

UNKNOWN = "Unknown"
UNKNOWN_PERFORMANCES = {"cpu_usage": UNKNOWN, "memory_usage": UNKNOWN}

CONNECTION_STATUS = "connection_status"
LAST_STATUS_UPDATE = "last_status_update"

COLUMNS = (
    ("mac_address", "Mac Address", 150),
    ("host", "Host", 150),
    ("hostname", "Hostname", 200),
    ("username", "Username", 150),
    (LAST_STATUS_UPDATE, "Last Status Update", 180),
    (CONNECTION_STATUS, "Connection Status", 150),
    ("cpu_usage", "CPU Usage (%)", 120),
    ("memory_usage", "RAM Usage (%)", 120),
    ("cpu_count", "CPU Count", 100),
    ("ram", "RAM (bytes)", 120),
    ("os", "OS", 200),
    ("cpu_name", "CPU Name", 350),
)

FIELDS = [field for field, _, _ in COLUMNS]

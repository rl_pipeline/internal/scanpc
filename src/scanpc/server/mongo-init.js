db.createUser({
    user: 'scanpc-user',
    pwd: 'scanpc-pass',
    roles: [
      {
        role: 'readWrite',
        db: 'scanpc'
      }
    ]
  })
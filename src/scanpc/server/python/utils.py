import json
import os
import threading

import tiffany

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception


logger = tiffany.get_logger(__name__)


def get_scanpc_schema_validator():
    with open(
        os.path.join(
            os.getenv("SCANPC_CONFIG_SERVER", "/usr/local/scanpc/config/server"),
            "validator.json",
        )
    ) as json_file:
        return json.load(json_file)


class WorkstationsResourceManager(object):
    def __init__(self, db):
        self._db = db
        self._lock = threading.Lock()

    def get_resources(self):
        """
        Returns:
            list[dict]
        """
        with self._lock:
            return self._db.get_workstations()

    def get_resource(self, primary_key):
        """
        Args:
            primary_key (str):

        Returns:
            dict:
        """
        with self._lock:
            return self._db.get_workstation(primary_key)
           
    def insert_resource(self, fields):
        """
        Args:
            fields (dict):
        """
        with self._lock:
            self._db.insert_workstation(fields)
         

    def update_resource(self, primary_key, fields):
        """
        Args:
            primary_key (str):
            fields (dict):

        Returns:
            dict: Update status data, i.e: 
                {"updatedExisting": True, "nModified": 1, "ok": 1.0, "n": 1}
        """
        with self._lock:
            return self._db.update_workstation(primary_key, fields)
          

    def delete_resources(self):
        with self._lock:
            self._db.delete_workstations()
            

    def delete_resource(self, primary_key):
        """
        Args:
            primary_key (str):

        Returns:
            dict: Delete status data, i.e: {"ok": 1.0, "n": 1}
        """
        with self._lock:
            return self._db.delete_workstation(primary_key)
          


class PerformancesResourceManager(object):
    def __init__(self, db):
        self._db = db
        self._lock = threading.Lock()

    def get_resources(self):
        """
        Returns:
            dict:
        """
        with self._lock:
            return self._db.get_performances()

    def get_resource(self, primary_key):
        """
        Args:
            primary_key (str):

        Returns:
            tuple[float, float]: (cpu_usage, memory_usage) in percent.
        """
        with self._lock:
            return self._db.get_performance(primary_key)

    def upsert_resource(self, primary_key, fields):
        """
        Args:
            primary_key (str):
            fields (dict):
        """
        with self._lock:
            self._db.upsert_performance(primary_key, fields)

    def delete_resources(self):
        with self._lock:
            self._db.delete_performances()

    def delete_resource(self, primary_key):
        """
        Args:
            primary_key (str):
        """
        with self._lock:
            self._db.delete_performance(primary_key)
           
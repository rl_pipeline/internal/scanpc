import collections

import flask_pymongo
import pymongo.errors as pymongo_errors
import tiffany

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception
import scanpc.server.utils as server_util


logger = tiffany.get_logger(__name__)


class WorkstationsDatabase(object):

    workstations_col = constant.WORKSTATIONS_COL

    def __init__(self, app):
        self._mongo = flask_pymongo.PyMongo(app=app)
        self._setup_workstations_collection(
            self._mongo, self.__class__.workstations_col
        )

    @staticmethod
    def _setup_workstations_collection(pymongo, workstations_col):
        if not workstations_col in pymongo.db.list_collection_names():
            pymongo.db.workstations.drop()
            pymongo.db.create_collection(workstations_col)
            pymongo.db.command(
                collections.OrderedDict(
                    [
                        ("collMod", workstations_col),
                        ("validator", server_util.get_scanpc_schema_validator()),
                        ("validationLevel", "strict"),
                    ]
                )
            )

    def insert_workstation(self, fields):
        try:
            fields["_id"] = fields.pop(constant.PRIMARY_KEY)
        except KeyError:
            raise server_exception.WorkstationsDatabaseFieldsError(
                "Missing required '{primary_key}': {fields}".format(
                    fields=fields, primary_key=constant.PRIMARY_KEY
                )
            )
        else:
            try:
                try:
                    self._mongo.db.workstations.insert_one(fields)
                except pymongo_errors.DuplicateKeyError:
                    self._mongo.db.workstations.replace_one(
                        {"_id": fields["_id"]}, fields
                    )
            except (pymongo_errors.OperationFailure, pymongo_errors.WriteError) as e:
                raise server_exception.WorkstationsDatabaseOperationalError(e)
            except pymongo_errors.PyMongoError as e:
                raise server_exception.WorkstationsDatabaseError(e)

    def get_workstations(self):
        workstations = []
        try:
            cursor = self._mongo.db.workstations.find()
        except pymongo_errors.PyMongoError as e:
            raise server_exception.WorkstationsDatabaseError(e)
        except pymongo_errors.OperationFailure as e:
            raise server_exception.WorkstationsDatabaseOperationalError(e)
        else:
            for workstation in cursor:
                workstation[constant.PRIMARY_KEY] = workstation.pop("_id")
                workstations.append(workstation)
            return workstations

    def delete_workstations(self):
        try:
            self._mongo.db.workstations.delete_many({})
        except pymongo_errors.PyMongoError as e:
            raise server_exception.WorkstationsDatabaseError(e)
        except pymongo_errors.OperationFailure as e:
            raise server_exception.WorkstationsDatabaseOperationalError(e)

    def get_workstation(self, primary_key):
        try:
            workstation = self._mongo.db.workstations.find_one({"_id": primary_key})
            workstation[constant.PRIMARY_KEY] = workstation.pop("_id")
            return workstation
        except pymongo_errors.PyMongoError as e:
            raise server_exception.WorkstationsDatabaseError(e)
        except pymongo_errors.OperationFailure as e:
            raise server_exception.WorkstationsDatabaseOperationalError(e)

    def update_workstation(self, primary_key, fields):
        try:
            result = self._mongo.db.workstations.update_one(
                {"_id": primary_key}, {"$set": fields}
            )
        except (pymongo_errors.OperationFailure, pymongo_errors.WriteError) as e:
            raise server_exception.WorkstationsDatabaseOperationalError(e)
        except pymongo_errors.PyMongoError as e:
            raise server_exception.WorkstationsDatabaseError(e)
        else:
            return result.raw_result

    def delete_workstation(self, primary_key):
        try:
            result = self._mongo.db.workstations.delete_one({"_id": primary_key})
        except pymongo_errors.OperationFailure as e:
            raise server_exception.WorkstationsDatabaseOperationalError(e)
        except pymongo_errors.PyMongoError as e:
            raise server_exception.WorkstationsDatabaseError(e)
        else:
            return result.raw_result


class PerformancesDatabase(object):
    def __init__(self, performance_data=None):
        self._performance_data = performance_data or {}

    def get_performances(self):
        return {
            primary_key: {
                "cpu_usage": data["cpu_usage"],
                "memory_usage": data["memory_usage"],
            }
            for primary_key, data in self._performance_data.items()
        }

    def get_performance(self, primary_key):
        try:
            return self._performance_data[primary_key]
        except KeyError as e:
            raise server_exception.PerformancesKeyError(e)

    def upsert_performance(self, primary_key, fields):
        try:
            self._performance_data[primary_key] = {
                "cpu_usage": fields["cpu_usage"],
                "memory_usage": fields["memory_usage"],
            }
        except KeyError as e:
            raise server_exception.PerformancesKeyError(
                "error: {}, fields: {}".format(e, fields)
            )
        else:
            return True

    def delete_performances(self):
        self._performance_data.clear()

    def delete_performance(self, primary_key):
        try:
            self._performance_data.pop(primary_key)
        except KeyError as e:
            raise server_exception.PerformancesKeyError("{}: {}".format(e, primary_key))

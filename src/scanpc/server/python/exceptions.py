class WorkstationsDatabaseError(Exception):
    pass


class WorkstationsDatabaseFieldsError(Exception):
    pass


class WorkstationsDatabaseOperationalError(Exception):
    pass


class PerformancesKeyError(Exception):
    pass

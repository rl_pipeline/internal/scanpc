import httplib
import os

import flask
import flask_restful
import tiffany
import tiffany.utils as tiffany_util
from flask import request
from flask_restful import reqparse

import scanpc.constants as constant
import scanpc.server.exceptions as server_exception


logger = tiffany.get_logger(__name__)


_PERFORMANCE_DATA = {}


def parse_post_args():
    parser = reqparse.RequestParser()
    parser.add_argument("mac_address", required=True, type=str, help="mac_address")
    parser.add_argument("hostname", required=True, type=str, help="hostname")
    parser.add_argument("username", required=True, type=str, help="username")
    parser.add_argument("host", required=True, type=str, help="host")
    parser.add_argument("os", required=True, type=str, help="os")
    parser.add_argument("ram", required=True, type=int, help="ram")
    parser.add_argument("cpu_count", required=True, type=int, help="cpu_count")
    parser.add_argument("cpu_name", required=True, type=str, help="cpu_name")
    return parser.parse_args()


def parse_put_args():
    parser = reqparse.RequestParser()
    parser.add_argument("mac_address", type=str, help="mac_address")
    parser.add_argument("hostname", type=str, help="hostname")
    parser.add_argument("username", type=str, help="username")
    parser.add_argument("host", type=str, help="host")
    parser.add_argument("os", type=str, help="os")
    parser.add_argument("ram", type=int, help="ram")
    parser.add_argument("cpu_count", type=int, help="cpu_count")
    parser.add_argument("cpu_name", type=str, help="cpu_name")
    return parser.parse_args()


def parse_performance_post_args():
    parser = reqparse.RequestParser()
    parser.add_argument(
        constant.PRIMARY_KEY, required=True, type=str, help=constant.PRIMARY_KEY
    )
    parser.add_argument("cpu_usage", required=True, type=float, help="cpu_usage")
    parser.add_argument("memory_usage", required=True, type=float, help="memory_usage")
    return parser.parse_args()


def parse_performance_put_args():
    parser = reqparse.RequestParser()
    parser.add_argument("cpu_usage", type=float, help="cpu_usage")
    parser.add_argument("memory_usage", type=float, help="memory_usage")
    return parser.parse_args()


class Workstations(flask_restful.Resource):

    manager = None

    def get(self):
        try:
            workstations = self.__class__.manager.get_resources()
        except server_exception.WorkstationsDatabaseError as e:
            msg = "GET - Workstations database error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseError as e:
            msg = "GET - Workstations database operational error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            if workstations:
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {resource}", resource=workstations)
                return workstations, httplib.OK
            else:
                msg = "No workstations found on database!"
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {}".format(msg))
                flask.abort(httplib.NOT_FOUND, msg)

    def post(self):
        args = parse_post_args()
        fields = args.copy()
        try:
            self.__class__.manager.insert_resource(fields)
        except server_exception.WorkstationsDatabaseFieldsError as e:
            logger.error(e)
            flask.abort(httplib.BAD_REQUEST, e)
        except server_exception.WorkstationsDatabaseError:
            msg = "POST - Workstations db error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseOperationalError:
            msg = "POST - Workstations db operational error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            if tiffany_util.is_debug():
                logger.debug("POST - Resource: {resource}", resource=args)
            return httplib.OK

    def delete(self):
        try:
            self.__class__.manager.delete_resources()
        except server_exception.WorkstationsDatabaseError:
            msg = "DELETE - Workstations db error!"
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseOperationalError:
            msg = "DELETE - Workstations db operational Error!"
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            if tiffany_util.is_debug():
                logger.debug("DELETE - Workstations have been deleted.")
            return httplib.OK


class Workstation(flask_restful.Resource):

    manager = None

    def get(self, primary_key):
        try:
            workstation = self.__class__.manager.get_resource(primary_key)
        except server_exception.WorkstationsDatabaseError as e:
            msg = "GET - Workstation db error: {}".format(e)
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseOperationalError as e:
            msg = "GET - Workstation db operational error: {}".format(e)
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            is_debug = tiffany_util.is_debug()
            if workstation:
                if is_debug:
                    logger.debug("GET - Resource: {resource}", resource=workstation)
                return workstation, httplib.OK
            else:
                msg = "GET - Workstation {} was not found in the collection!".format(
                    primary_key
                )
                if is_debug:
                    logger.debug(msg)
                flask.abort(httplib.NOT_FOUND, msg)

    def put(self, primary_key):
        args = parse_put_args()
        args = {k: v for k, v in args.items() if not v is None}
        if not args:
            flask.abort(httplib.NOT_ACCEPTABLE, "NOT ACCEPTABLE - Fields is empty!")

        try:
            result = self.__class__.manager.update_resource(primary_key, args)
        except server_exception.WorkstationsDatabaseError:
            msg = "PUT - Workstation db error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseOperationalError:
            msg = "PUT - Workstation db operational error!"
            logger.critical(msg)
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            if result["nModified"] == 1:
                args["url"] = request.base_url
                if tiffany_util.is_debug():
                    logger.debug("PUT - Resource: {resource}", resource=args)
                return args, httplib.OK
            else:
                return httplib.NO_CONTENT

    def delete(self, primary_key):
        try:
            result = self.__class__.manager.delete_resource(primary_key)
        except server_exception.WorkstationsDatabaseError:
            msg = "DELETE - Workstation db Error!"
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        except server_exception.WorkstationsDatabaseOperationalError:
            msg = "DELETE - Workstation db operational Error!"
            flask.abort(httplib.INTERNAL_SERVER_ERROR, msg)
        else:
            if result["ok"] == 1.0 and result["n"] == 1:
                msg = "DELETE - Resource: {}".format(request.base_url)
                if tiffany_util.is_debug():
                    logger.debug(msg)
                return httplib.OK
            else:
                msg = "DELETE - NO CONTENT: {}".format(request.base_url)
                logger.debug(msg)
                return httplib.NOT_FOUND


class Performances(flask_restful.Resource):

    manager = None

    def get(self):
        performances = self.__class__.manager.get_resources()
        if performances:
            if tiffany_util.is_debug():
                logger.debug("GET - Resource: {resource}", resource=performances)
            return performances, httplib.OK
        else:
            msg = "GET - No performances data available!"
            if tiffany_util.is_debug():
                logger.debug(msg)
            flask.abort(httplib.NOT_FOUND, msg)

    def post(self):
        args = parse_performance_post_args()
        primary_key = args.pop(constant.PRIMARY_KEY)
        try:
            self.__class__.manager.upsert_resource(primary_key, args)
        except server_exception.PerformancesKeyError:
            msg = "POST - Missing key error: {}".format(primary_key)
            logger.error(msg)
            flask.abort(httplib.NOT_FOUND, msg)
        else:
            if tiffany_util.is_debug():
                logger.debug("POST - Resource: {resource}", resource=args)
            return httplib.OK

    def delete(self):
        self.__class__.manager.delete_resources()
        if tiffany_util.is_debug():
            logger.debug("DELETE - Performance data have been deleted.")
        return httplib.OK


class Performance(flask_restful.Resource):

    manager = None

    def get(self, primary_key):
        try:
            data = self.__class__.manager.get_resource(primary_key)
        except server_exception.PerformancesKeyError:
            msg = "GET - Missing key error: {}".format(primary_key)
            logger.error(msg)
            flask.abort(httplib.NOT_FOUND, msg)
        else:
            is_debug = tiffany_util.is_debug()
            if data:
                if is_debug:
                    logger.debug("GET - Resource: {resource}", resource=data)
                return data, httplib.OK
            else:
                msg = "GET - {} was not found in the collection!".format(primary_key)
                if is_debug:
                    logger.debug(msg)
                flask.abort(httplib.NOT_FOUND, msg)

    def put(self, primary_key):
        args = parse_performance_put_args()
        try:
            self.__class__.manager.upsert_resource(primary_key, args)
        except server_exception.PerformancesKeyError:
            msg = "PUT - Missing key error: {}".format(primary_key)
            logger.error(msg)
            flask.abort(httplib.NOT_FOUND, msg)
        else:
            if tiffany_util.is_debug():
                logger.debug("PUT - Resource: {resource}", resource=args)
            return args, httplib.OK

    def delete(self, primary_key):
        try:
            self.__class__.manager.delete_resource(primary_key)
        except erver_exception.PerformancesKeyError:
            msg = "DELETE - Missing key error: {}".format(primary_key)
            logger.error(msg)
            flask.abort(httplib.NOT_FOUND, msg)
        else:
            if tiffany_util.is_debug():
                logger.debug("DELETE - Resource: {}".format(request.base_url))
            return httplib.OK

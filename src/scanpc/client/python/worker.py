import os
import socket
import threading
import time
import sys
import signal

import psutil
import requests
import tiffany
import tiffany.utils as tiffany_util

import scanpc.constants as constant
import scanpc.utils as util

logger = tiffany.get_logger(__name__)


class PerformanceThreading(threading.Thread):
    def __init__(self, host, port, interval, primary_key):
        """
        Args:
            host (str):
            port (int):
            interval (int): Time interval in second for sending the UDP packet.
            primary_key (str):

        Returns:
            dict[str: dict]
        """
        super(PerformanceThreading, self).__init__()
        self._event = threading.Event()
        self._server_host = host
        self._server_port = port
        self._primary_key = primary_key
        self._url = "{url_scheme}{host}:{port}{path}".format(
            url_scheme=constant.URL_SCHEME,
            host=host,
            port=port,
            path=constant.PERFORMANCES_PATH,
        )
        self._host = socket.gethostbyname(socket.gethostname())
        self.daemon = True
        self.interval = interval

    @property
    def event(self):
        return self._event

    def run(self):
        logger.info(
            "PID {pid} - Start performance updater {host}",
            pid=os.getpid(),
            host=self._host,
        )
        logger.info(
            "Performance updater interval is set to {} second(s)".format(self.interval)
        )
        while self._event.isSet():
            respond = requests.post(
                self._url,
                {
                    constant.PRIMARY_KEY: self._primary_key,
                    "cpu_usage": psutil.cpu_percent(),
                    "memory_usage": psutil.virtual_memory()[2],
                },
            )
            if tiffany_util.is_debug():
                logger.debug(
                    "{data} was sent to {url_path} at {time}",
                    data=respond.json(),
                    url_path=self._url,
                    time=util.get_datetime_now(),
                )
            time.sleep(self.interval)

    def stop(self, signal, frame):
        logger.info("Stop performance updater {}".format(self._host))
        self._event.clear()
        # Wait until the thread terminates.
        self.join()


def get_performance_worker(host, port, interval, primary_key):
    """Factory function to return SenderThreading instance.

    Args:
        host (str):
        port (int):
        interval (int): Time interval in second for sending the UDP packet.
        primary_key (str):

    Returns:
        SenderThreading
    """
    worker_thread = PerformanceThreading(host, port, float(interval), primary_key)
    worker_thread.event.set()
    return worker_thread

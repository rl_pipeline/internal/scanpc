import os
import sys
import threading
import time

import tiffany
from Qt import QtWidgets, QtGui, QtCore

import scanpc.constants as constant
import scanpc.client.entity.output as client_output
import scanpc.client.ui.constants as ui_constant
import scanpc.client.ui.model as ui_model
import scanpc.client.ui.widgets as ui_widgets
import scanpc.utils as util


logger = tiffany.get_logger(__name__)


class RoutineWorker(QtCore.QThread):

    dataReady = QtCore.Signal(list)

    def __init__(self, output_entity):
        super(RoutineWorker, self).__init__()
        self._output_entity = output_entity

    def run(self):
        time.sleep(1)
        while True:
            self.dataReady.emit(self._output_entity.get_workstations_data())
            time.sleep(float(os.getenv("BEATBOT_UPDATER_TIMEOUT", 30)))


class ScanpcMonitorWindow(QtWidgets.QWidget):
    def __init__(self, output_entity, parent=None):
        super(ScanpcMonitorWindow, self).__init__(parent=parent)
        self._output_entity = output_entity
        self._model = ui_model.get_model(output_entity)
        self.setWindowTitle("Scanpc Monitor")
        self.setObjectName("ScanpcMonitor")
        self.setWindowFlags(QtCore.Qt.Window)
        self.build_ui()
        self.routine_worker = RoutineWorker(output_entity)
        self.routine_worker.dataReady.connect(self._model.on_data_ready)
        self.routine_worker.dataReady.connect(self.on_data_ready)
        self.routine_worker.start()
        self._is_header_stretched = False

    def _adjust_horizonal_header(self):
        self._table_view.horizontalHeader().setSectionResizeMode(
            len(constant.COLUMNS) - 1, QtWidgets.QHeaderView.Stretch
        )
        for i, column_data in enumerate(constant.COLUMNS):
            _, _, col_width = column_data
            self._table_view.setColumnWidth(i, col_width)
        self._is_header_stretched = True

    def sizeHint(self):
        return QtCore.QSize(*ui_constant.DEAULT_WINDOW_SIZE_HINT)

    def build_ui(self):
        main_layout = QtWidgets.QVBoxLayout()
        self._scroll_area = QtWidgets.QScrollArea(self)
        self._table_view = ui_widgets.ScanPCMonitorTableView(self._scroll_area)
        self._table_view.setModel(self._model)
        if not self._table_view.horizontalHeader().length() == 0:
            self._adjust_horizonal_header()

        self._scroll_area.setWidget(self._table_view)
        main_layout.addWidget(self._table_view)
        self.setLayout(main_layout)

    def on_data_ready(self, workstations_data):
        if len(workstations_data) == 0 or self._is_header_stretched:
            return
        self._adjust_horizonal_header()


def launch_standalone_window(output_entity):
    app = QtWidgets.QApplication(sys.argv)
    with open("{}/style.css".format(util.get_config_directory()), "r") as style_file:
        style_sheet = style_file.read().replace("\n", "")

    app.setStyleSheet(style_sheet)
    window = ScanpcMonitorWindow(output_entity)
    window.show()
    return app.exec_()

from Qt import QtWidgets, QtCore, QtGui

import scanpc.client.ui.constants as ui_constant


class ScanPCMonitorTableView(QtWidgets.QTableView):
    def __init__(self, parent):
        super(ScanPCMonitorTableView, self).__init__(parent)
        self.verticalHeader().hide()

    def sizeHint(self):
        model = self.model()
        if not model:
            return QtCore.QSize(*ui_constant.DEAULT_WINDOW_SIZE_HINT)

        total_column_width = 0

        total_column_width = sum(
            self.columnWidth(i) for i in range(model.columnCount())
        )

        margins = self.contentsMargins()
        top_bottom_margins = margins.top() + margins.bottom()
        return QtCore.QSize(
            total_column_width + self.horizontalHeader().sizeHint().width(),
            ui_constant.DEAULT_WINDOW_SIZE_HINT[1],
        )

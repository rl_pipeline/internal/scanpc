import datetime

import tiffany
from Qt import QtCore, QtGui

import scanpc.constants as constant
import scanpc.client.ui.constants as ui_constant


logger = tiffany.get_logger(__name__)


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, table_data=None, headers=None, parent=None):
        super(TableModel, self).__init__(parent)
        self._table_data = table_data or [[]]
        self._header_data = headers or [[], []]

    @property
    def table_data(self):
        return self._table_data

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._table_data)

    def columnCount(self, parent=QtCore.QModelIndex()):
        try:
            return len(self._table_data[0])
        except:
            return 0

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if self._header_data[0]:
                    return self._header_data[0][section]
            if orientation == QtCore.Qt.Vertical:
                if self._header_data[1]:
                    return self._header_data[1][section]
        if role == QtCore.Qt.ForegroundRole:
            return QtGui.QBrush(QtCore.Qt.white)

        return super(TableModel, self).headerData(section, orientation, role)

    def flags(self, index=QtCore.QModelIndex()):
        return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return

        column = index.column()
        item = self._table_data[index.row()][column]
        conn_status = self._table_data[index.row()][
            constant.FIELDS.index(constant.CONNECTION_STATUS)
        ]
        if role == QtCore.Qt.DisplayRole:
            return str(item)
        if role == QtCore.Qt.EditRole:
            return str(item)
        if role == QtCore.Qt.ForegroundRole:
            if conn_status == constant.CONNECTED:
                return QtGui.QBrush(QtCore.Qt.green)
            else:
                return QtGui.QBrush(QtCore.Qt.gray)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if not index.isValid():
            return

        self._table_data[index.row()][index.column()] = value
        self.dataChanged.emit(index, index, 0)
        return True

    def on_data_ready(self, workstations_data):
        self.beginResetModel()
        self._table_data = workstations_data
        self.endResetModel()
        self.layoutChanged.emit()


def get_model(output_entity):
    """Factory function to instantiate the table model.

    Args:
        output_entity (scanpc.client.entity.output.OutputEntity)

    Returns:
        scanpc.client.ui.model.TableModel
    """
    workstations_data = output_entity.get_workstations_data()
    headers = [header for _, header, _ in constant.COLUMNS]
    table_model = TableModel(table_data=workstations_data, headers=[headers, []])
    return table_model

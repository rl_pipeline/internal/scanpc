import beatbot.client.entity.output as beatbot_output

import scanpc.constants as constant


__all__ = ["AbstractEntity"]


class AbstractEntity(object):
    def __init__(
        self,
        host,
        port=constant.SCANPC_SERVER_DEFAULT_PORT,
        beatbot_port=constant.BEATBOT_SERVER_DEFAULT_PORT,
    ):
        self._domain = "{scheme}{host}:{port}".format(
            scheme=constant.URL_SCHEME, host=host, port=port,
        )

        self._beatbot_output = beatbot_output.OutputEntity(host=host, port=beatbot_port)

    @property
    def domain(self):
        return self._domain

    @property
    def beatbot_output(self):
        return self._beatbot_output

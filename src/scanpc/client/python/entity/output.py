import httplib
import json

import requests
import tiffany
import tiffany.utils as tiffany_util


import scanpc.constants as constant
import scanpc.utils as util
from scanpc.client.entity import abstract


logger = tiffany.get_logger(__name__)


__all__ = ["OutputEntity"]


class OutputEntity(abstract.AbstractEntity):
    def get_workstations(self):
        """Return all registered workstations data.

        Returns:
            list[dict]
        
        .. code-block:: python
            [
                {
                    'cpu_count': 12,
                    'cpu_name': u'Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz',
                    'host': u'192.168.0.32',
                    'hostname': u'rickylinton-desktop',
                    'mac_address': u'28:c6:3f:1b:61:24',
                    'os': u'CentOS Linux-8.5.2111-',
                    'ram': 16742858752,
                    'username': u'rickylinton'
                }
            ]
        """
        try:
            respond = requests.get(
                "{domain}{path}".format(
                    domain=self.domain, path=constant.WORKSTATIONS_PATH
                )
            )
        except requests.exceptions.ConnectionError as e:
            logger.critical(
                "Unknown service, connection error to: {domain}{path}",
                domain=self.domain,
                path=constant.WORKSTATIONS_PATH,
                error=e,
            )
            raise
        else:
            if respond.status_code == httplib.OK:
                if tiffany_util.is_debug():
                    logger.debug(
                        "GET - workstation data was found in the workstation collection!"
                    )
                return respond.json()
            elif respond.status_code == httplib.NOT_FOUND:
                if tiffany_util.is_debug():
                    logger.debug(
                        "GET - workstation data was not found in the workstation collection!"
                    )
                return []
            else:
                raise NotImplementedError(
                    "Respond status handler has not been implemented: {}".format(
                        respond.status_code
                    )
                )

    def get_workstation(self, primary_key):
        """Return requested workstation data.

        Args:
            primary_key (str): Workstation IP address.

        Returns:
            dict

        .. code-block:: python

            {
                'cpu_count': 12,
                'cpu_name': u'Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz',
                'host': u'192.168.0.32',
                'hostname': u'rickylinton-desktop',
                'mac_address': u'28:c6:3f:1b:61:24',
                'os': u'CentOS Linux-8.5.2111-',
                'ram': 16742858752,
                'username': u'rickylinton'
            }
        """
        try:
            respond = requests.get(
                "{domain}{path}/{primary_key}".format(
                    domain=self.domain,
                    path=constant.WORKSTATIONS_PATH,
                    primary_key=primary_key,
                )
            )
        except requests.exceptions.ConnectionError as e:
            logger.critical(
                "Unknown service, connection error to: {domain}{path}/{primary_key}",
                domain=self.domain,
                path=constant.WORKSTATIONS_PATH,
                primary_key=primary_key,
                error=e,
            )
            raise
        else:
            if respond.status_code == httplib.NOT_FOUND:
                if tiffany_util.is_debug():
                    logger.debug(
                        "GET - {primary_key} was not found in the workstation collection!",
                        primary_key=primary_key,
                    )
                return {}
            else:
                resource = respond.json()
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {resource}", resource=resource)
                return resource

    def get_performances(self):
        """Return all registered performances data.

        Returns:
            dict[dict]

        .. code-block:: python

            {"192.168.0.32": {"cpu_usage': 9.0, "memory_usage": 20.7}}
        """
        try:
            respond = requests.get(
                "{domain}{path}".format(
                    domain=self.domain, path=constant.PERFORMANCES_PATH
                )
            )
        except requests.exceptions.ConnectionError as e:
            logger.critical(
                "Unknown service, connection error to: {domain}{path}",
                domain=self.domain,
                path=constant.PERFORMANCES_PATH,
                error=e,
            )
            raise
        else:
            if respond.status_code == httplib.OK:
                return respond.json()
            elif respond.status_code == httplib.NOT_FOUND:
                return {}
            else:
                raise NotImplementedError(
                    "Respond status handler has not been implemented: {}".format(
                        respond.status_code
                    )
                )

    def get_performance(self, primary_key):
        """Return requested performance data.

        Args:
            primary_key (str): Workstation IP address.

        Returns:
            dict

        .. code-block:: python

            {"cpu_usage': 9.0, "memory_usage": 20.7}
        """
        try:
            respond = requests.get(
                "{domain}{path}/{primary_key}".format(
                    domain=self.domain,
                    path=constant.PERFORMANCES_PATH,
                    primary_key=primary_key,
                )
            )
        except requests.exceptions.ConnectionError as e:
            logger.critical(
                "Unknown service, connection error to: {domain}{path}/{primary_key}",
                domain=self.domain,
                path=constant.PERFORMANCES_PATH,
                primary_key=primary_key,
                error=e,
            )
            raise
        else:
            if respond.status_code == httplib.NOT_FOUND:
                if tiffany_util.is_debug():
                    logger.debug(
                        "GET - {primary_key} was not found in the performance data!",
                        primary_key=primary_key,
                    )
                return {}
            else:
                resource = respond.json()
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {resource}", resource=resource)
                return resource

    def get_workstations_data(self):
        """Get workstations data based on the order of the column fields.
        This is useful for the table model data.

        Returns:
            list
        
         .. code-block:: python

            [
                [
                    '28:c6:3f:1b:61:24',
                    '192.168.0.32',
                    'rickylinton-desktop',
                    'rickylinton',
                    '2022-10-04 22:13:26',
                    'Connected',
                    '6.5',
                    '22.4',
                    '12',
                    '16742858752',
                    'CentOS Linux-8.5.2111-',
                    'Intel(R) Core(TM) i7-8700K CPU @ 3.70GHz'
                ]
            ]
        """
        workstations = self.get_workstations()
        for workstation in workstations:
            try:
                primary_key = workstation[constant.PRIMARY_KEY]
            except IndexError:
                logger.critical(
                    "Primary key {key} does not exist in workstation object!",
                    key=constant.PRIMARY_KEY,
                    workstation=workstation,
                )
                raise
            else:
                heartbeat = self.beatbot_output.get_heartbeat(primary_key)
                if heartbeat.get("state", False):
                    workstation.update(
                        {constant.CONNECTION_STATUS: constant.CONNECTED}
                    )
                    workstation.update(
                        {
                            constant.LAST_STATUS_UPDATE: util.get_datetime_from_timestamp(
                                heartbeat["last_connected"]
                            )
                        }
                    )
                    performance = self.get_performance(primary_key)
                    if performance:
                        workstation.update(performance)
                    else:
                        logger.warning(
                            "Host '{host}' is connected, but performance is unknown!",
                            host=workstation["host"],
                        )
                        workstation.update(constant.UNKNOWN_PERFORMANCES)
                else:
                    workstation.update(
                        {constant.CONNECTION_STATUS: constant.NOT_CONNECTED}
                    )
                    last_connected = heartbeat.get("last_connected", constant.UNKNOWN)
                    if last_connected != constant.UNKNOWN:
                        last_connected = util.get_datetime_from_timestamp(last_connected)
                    workstation.update({constant.LAST_STATUS_UPDATE: last_connected})
                    workstation.update(constant.UNKNOWN_PERFORMANCES)
        
        return [
            [workstation[field] for field in constant.FIELDS]
            for workstation in workstations
        ]

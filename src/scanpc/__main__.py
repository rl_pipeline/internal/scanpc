import argparse
import logging
import os
import threading
import signal
import sys
import time

import tiffany
import tiffany.utils as tiffany_util

import scanpc.constants as constant
import scanpc.utils as utils


logger = tiffany.get_logger(__name__)


def parse_argument():
    parser = argparse.ArgumentParser(
        prog="scanpc",
        epilog=("Example:\n" "   scanpc server\n" "   scanpc --host my_host --port 6000 server\n" "   scanpc client\n" "   scanpc monitor\n"),
        formatter_class=argparse.RawTextHelpFormatter,
    )
    subparser = parser.add_subparsers()
    parser.add_argument("--host", type=str, default="0.0.0.0", help="Host address.")
    parser.add_argument("--port", type=int, default=5000, help="Port number.")
    server_subparser = subparser.add_parser("server", help="Server sub-command.")
    server_subparser.set_defaults(func=_start_server)

    client_subparser = subparser.add_parser("client", help="Client sub-command.")
    client_subparser.set_defaults(func=_start_client)
    client_monitor_subparser = subparser.add_parser(
        "monitor", help="Client monitor sub-command."
    )
    client_monitor_subparser.set_defaults(func=_start_client_monitor)
    return parser.parse_args()


def _start_flask_app(app, host, port):
    import waitress


    if eval(os.getenv("SCANPC_DEVELOPMENT", False)):
        # Set use_reloader to False to avoid "signal only works in the main thread" issue.
        # https://stackoverflow.com/questions/53522052/flask-app-valueerror-signal-only-works-in-main-thread
        app.run(host=host, port=port, use_reloader=False, threaded=True)
    else:
        waitress.serve(app, host=host, port=port)


def _validate_timeout_and_interval_valid():
    interval = int(os.getenv("BEATBOT_SENDER_INTERVAL", 30))
    timeout = int(os.getenv("BEATBOT_UPDATER_TIMEOUT", 15))
    if not interval < timeout:
        msg = "Timeout ({}) is set smaller than the interval ({})!".format(
            timeout, interval
        )
        logger.error(msg, timeout=timeout, interval=interval)
        raise ValueError(msg)


def _start_server(args):
    import flask
    import flask_restful
    import waitress

    import scanpc.server.database as server_db
    import scanpc.server.api as server_api
    import scanpc.server.utils as server_util

    
    app = flask.Flask(__name__)
    app.config["MONGO_URI"] = (
        "mongodb://{username}:{password}@{service_name}:{port}/{database}".format(
            username=os.getenv('MONGO_USER_NAME'),
            password=os.getenv('MONGO_USER_PASSWORD'),
            database=constant.DATABASE,
            port=int(os.getenv('MONGO_PORT', 27017)),
            service_name=os.getenv('MONGO_SERVICE_NAME')
        )
    )

    api = flask_restful.Api(app)
    api.add_resource(
        server_api.Workstations,
        "{workstations_path}".format(workstations_path=constant.WORKSTATIONS_PATH)
    )
    api.add_resource(
        server_api.Performances,
        "{performances_path}".format(performances_path=constant.PERFORMANCES_PATH)
    )
    api.add_resource(
        server_api.Workstation, 
        "{workstations_path}/<string:primary_key>".format(
            workstations_path=constant.WORKSTATIONS_PATH,
        )
    )
    api.add_resource(
        server_api.Performance, 
        "/{performances_path}/<string:primary_key>".format(
            performances_path=constant.PERFORMANCES_PATH,
        )
    )

    workstations_model = server_db.WorkstationsDatabase(app)
    workstations_manager = server_util.WorkstationsResourceManager(workstations_model)
    server_api.Workstations.manager = workstations_manager
    server_api.Workstation.manager = workstations_manager

    performances_model = server_db.PerformancesDatabase()
    performances_manager = server_util.PerformancesResourceManager(performances_model)
    server_api.Performances.manager = performances_manager
    server_api.Performance.manager = performances_manager

    flask_thread = threading.Thread(
        name="scanpc_flask_app_threading",
        target=_start_flask_app,
        args=(
            app,
            args.host,
            os.getenv("SCANPC_PORT", 5000),
        ),
    )
    flask_thread.setDaemon(True)
    flask_thread.start()

    try:
        while flask_thread.is_alive():
            # This is to suppress the cpu utilization
            time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(1)


def _start_client(args):
    import requests

    import scanpc.constants as constant
    import scanpc.client.worker as client_worker
    

    workstation = utils.Workstation()
    requests.post(
        "{url_scheme}{host}:{port}{path}".format(
            url_scheme=constant.URL_SCHEME, 
            host=args.host, 
            port=args.port,
            path=constant.WORKSTATIONS_PATH
        ),
        workstation.fields
    )

    performance_thread = client_worker.get_performance_worker(
        args.host, args.port, eval(os.getenv("BEATBOT_SENDER_INTERVAL", 15)), workstation.host
    )
    signal.signal(signal.SIGINT, performance_thread.stop)
    performance_thread.start()
    try:
        while performance_thread.is_alive():
            # This is to suppress the cpu utilization
            time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(1)


def _start_client_monitor(args):
    import scanpc.client.ui.view as client_view
    import scanpc.client.entity.output as client_output
    

    sys.exit(
        client_view.launch_standalone_window(
            client_output.OutputEntity(args.host, port=args.port, )
        )
    )


def main():
    args = parse_argument()
    if tiffany_util.is_debug():
        logger.set_stream_handler_level(logging.DEBUG)
        logger.debug("Listener - DEBUG MODE")
    else:
        logger.set_stream_handler_level(logging.INFO)
    args.func(args)
    return 0


if __name__ == "__main__":
    sys.exit(main())
import datetime
import getpass
import multiprocessing
import platform
import os
import socket

import cpuinfo
import psutil
import pytz
import scapy.all

import scanpc.constants as constant


def get_datetime_now():
    return datetime.datetime.now(
        pytz.timezone(os.getenv("TIMEZONE", constant.TIMEZONE))
    ).strftime(os.getenv("TIME_FORMAT", constant.TIME_FORMAT))


def get_datetime_from_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime(os.getenv("TIME_FORMAT", constant.TIME_FORMAT))


def get_host_data():
    hostname = socket.gethostname()
    return hostname, socket.gethostbyname(hostname)


def get_config_directory():
    return os.path.join(
        os.getenv("SCANPC_ROOT", "/usr/local/scanpc"),
        "config"
    )


class Workstation(object):

    def __init__(self):
        self.__mac_address = os.getenv("HOST_LOCAL_MAC_ADDRESS", None) or scapy.all.Ether().src
        self.__hostname = socket.gethostname()
        self.__host = os.getenv("HOST_LOCAL_IP", None) or socket.gethostbyname(self.__hostname)
        self.__username = getpass.getuser()
        self.__os = "-".join(platform.linux_distribution())
        self.__ram = psutil.virtual_memory().total
        self.__cpu_count = multiprocessing.cpu_count()
        self.__cpu_name = cpuinfo.get_cpu_info()["brand_raw"]

    def __str__(self):
        return (
            "Workstation"
                "("
                    "mac_address={mac}, "
                    "hostname={hostname}, "
                    "username={username}, "
                    "host={host}, "
                    "os={os}, "
                    "ram={ram}, "
                    "cpu_count={cpu_count}, "
                    "cpu_name={cpu_name}, "
                ")"
        ).format(
            mac=self.__mac_address, 
            hostname=self.__hostname, 
            host=self.__host,
            username=self.__username,
            os=self.__os,
            ram=self.__ram,
            cpu_count=self.__cpu_count,
            cpu_name=self.__cpu_name,
        )

    @property
    def mac_address(self):
        return self.__mac_address

    @property
    def hostname(self):
        return self.__hostname

    @property
    def username(self):
        return self.__username

    @property
    def host(self):
        return self.__host

    @property
    def os(self):
        return self.__os
    
    @property
    def ram(self):
        return self.__ram

    @property
    def cpu_count(self):
        return self.__cpu_count

    @property
    def cpu_name(self):
        return self.__cpu_name

    @property
    def fields(self):
        return {
            "mac_address": self.__mac_address, 
            "hostname": self.__hostname, 
            "host": self.__host,
            "username": self.__username,
            "os": self.__os,
            "ram": self.__ram,
            "cpu_count": self.__cpu_count,
            "cpu_name": self.__cpu_name,
        }

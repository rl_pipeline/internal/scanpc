# Build stage 1
FROM registry.gitlab.com/rl_pipeline/internal/cmake_modules:1.0.1 as cmake_modules
FROM registry.gitlab.com/rl_pipeline/internal/pylibs/tiffany:0.2.0 as tiffany
FROM registry.gitlab.com/rl_pipeline/internal/docker/centos-build:0.3.0 as centos-build

ARG CMAKE_MODULES_ROOT
ARG TIFFANY_ROOT

COPY --from=cmake_modules $CMAKE_MODULES_ROOT $CMAKE_MODULES_ROOT
COPY --from=tiffany $TIFFANY_ROOT $TIFFANY_ROOT

WORKDIR /home/dev/scanpc

COPY . .

RUN python2 -m pip install -r ./src/scanpc/server/requirements.txt

ENV PYTHONPATH=$TIFFANY_ROOT/lib64/python2.7/site-packages:$PYTHONPATH

RUN mkdir build \
    && cd build \
    && cmake -DSERVER_BUILD=True .. \
    # && ctest -V \
    && make install \
    && cd /home/dev/scanpc && rm -r build

# Build stage 2
FROM centos:centos8

ARG SCANPC_ROOT
ARG TIFFANY_ROOT
ARG PYTHON_ROOT
ARG TIFFANY_FILE_LOG_ROOT

COPY --from=centos-build $SCANPC_ROOT $SCANPC_ROOT
COPY --from=centos-build $PYTHON_ROOT $PYTHON_ROOT
COPY --from=centos-build $TIFFANY_ROOT $TIFFANY_ROOT

RUN groupadd scanpc -g 1000 \
    && useradd -g scanpc scanpc -u 1000 -s /bin/bash \
    && mkdir $TIFFANY_FILE_LOG_ROOT \
    && chown -R scanpc:scanpc $SCANPC_ROOT \
    && chown -R scanpc:scanpc $TIFFANY_FILE_LOG_ROOT

ENV TIFFANY_FILE_LOG_ROOT=$TIFFANY_FILE_LOG_ROOT

ENV PATH=$SCANPC_ROOT/bin:$PYTHON_ROOT/bin:$PATH

ENV PYTHONPATH=$SCANPC_ROOT/lib64/python2.7/site-packages:\
$TIFFANY_ROOT/lib64/python2.7/site-packages:\
$PYTHON_ROOT/lib/python2.7/site-packages:\
$PYTHONPATH

ENV LD_LIBRARY_PATH=$PYTHON_ROOT/lib:$LD_LIBRARY_PATH

USER scanpc